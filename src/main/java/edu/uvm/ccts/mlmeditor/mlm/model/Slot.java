/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MLM Editor.
 *
 * MLM Editor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MLM Editor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MLM Editor.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.mlmeditor.mlm.model;

/**
 * Created by mstorer on 4/2/14.
 */
public class Slot {
    private String name;
    private String data;

    public Slot(String name, String data) {
        this.name = name.trim();
        this.data = unindent(data);
    }

    public String getName() {
        return name;
    }

    public String getData() {
        return data;
    }

    private String unindent(String str) {
        if (str == null) return null;

        if (str.contains("\n")) {
            int indent = getIndent(str);
            if (indent > 0) {
                StringBuilder sb = new StringBuilder();
                for (String line : str.split("\n")) {
                    if (line.trim().length() == 0) {
                        sb.append("\n");

                    } else {
                        sb.append(line.substring(indent)).append("\n");
                    }
                }
                return sb.toString().trim();
            }
        }

        return str.trim();
    }

    private int getIndent(String str) {
        if (str == null) return 0;

        String[] lines = str.split("\n");
        int minIndent = -1;

        for (String line : lines) {
            if (line.trim().length() > 0) {                     // only care about lines with text
                int indent = 0;

                for (char c : line.toCharArray()) {             // find this line's indentation
                    if (c != ' ') break;
                    indent ++;
                }

                if (minIndent == -1 || indent < minIndent) {    // want to keep the smallest indentation
                    minIndent = indent;
                }
            }
        }

        return Math.max(minIndent, 0);
    }
}
