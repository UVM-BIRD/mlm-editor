/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MLM Editor.
 *
 * MLM Editor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MLM Editor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MLM Editor.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.mlmeditor.mlm.model;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mstorer on 4/2/14.
 */
public class MLM {
    private Map<String, Category> categories = new LinkedHashMap<String, Category>();

    public MLM(List<Category> categories) {
        for (Category cat : categories) {
            this.categories.put(cat.getName().toLowerCase(), cat);
        }
    }

    public Category getCategory(String name) {
        return categories.get(name.toLowerCase());
    }
}
