/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MLM Editor.
 *
 * MLM Editor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MLM Editor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MLM Editor.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.mlmeditor.mlm;

import edu.uvm.ccts.arden.mlm.antlr.MLMBaseVisitor;
import edu.uvm.ccts.arden.mlm.antlr.MLMParser;
import edu.uvm.ccts.mlmeditor.mlm.model.Category;
import edu.uvm.ccts.mlmeditor.mlm.model.MLM;
import edu.uvm.ccts.mlmeditor.mlm.model.Slot;
import org.antlr.v4.runtime.misc.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstorer on 4/2/14.
 */
public class MLMEvalVisitor extends MLMBaseVisitor<Boolean> {
    private List<Category> categories = new ArrayList<Category>();

    public MLM getMLM() {
        return new MLM(categories);
    }

    @Override
    public Boolean visitCategory(@NotNull MLMParser.CategoryContext ctx) {
        String categoryId = stripTrailing(ctx.CategoryID().getText(), ":");

        categories.add(new Category(categoryId));

        visitChildren(ctx);

        return super.visitCategory(ctx);
    }

    @Override
    public Boolean visitSlot(@NotNull MLMParser.SlotContext ctx) {
        String slotId = stripTrailing(ctx.SlotID().getText(), ":");
        String data = stripTrailing(ctx.Data().getText(), ";;");

        categories.get(categories.size() - 1).addSlot(new Slot(slotId, data));

        return super.visitSlot(ctx);
    }

    private String stripTrailing(String s, String toRemove) {
        if (s != null && s.endsWith(toRemove)) {
            return s.substring(0, s.length() - toRemove.length());

        } else {
            return s;
        }
    }
}
