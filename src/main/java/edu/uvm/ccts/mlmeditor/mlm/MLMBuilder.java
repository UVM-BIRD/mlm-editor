/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MLM Editor.
 *
 * MLM Editor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MLM Editor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MLM Editor.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.mlmeditor.mlm;

import edu.uvm.ccts.arden.mlm.BailMLMLexer;
import edu.uvm.ccts.arden.mlm.BailMLMParser;
import edu.uvm.ccts.arden.mlm.antlr.MLMLexer;
import edu.uvm.ccts.arden.mlm.antlr.MLMParser;
import edu.uvm.ccts.mlmeditor.mlm.model.MLM;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by mstorer on 4/2/14.
 */
public class MLMBuilder {
    public MLM build(File file) throws IOException {
        MLMEvalVisitor visitor = new MLMEvalVisitor();
        visitor.visit(buildParseTree(file));
        return visitor.getMLM();
    }

    private ParseTree buildParseTree(File file) throws IOException {
        ANTLRInputStream input = new ANTLRInputStream(new FileInputStream(file));
        MLMLexer lexer = new BailMLMLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MLMParser parser = new BailMLMParser(tokens);
        return parser.init();
    }
}
