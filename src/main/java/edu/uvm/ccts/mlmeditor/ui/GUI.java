/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MLM Editor.
 *
 * MLM Editor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MLM Editor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MLM Editor.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Created by JFormDesigner on Thu Mar 27 16:11:00 EDT 2014
 */

package edu.uvm.ccts.mlmeditor.ui;

import edu.uvm.ccts.common.util.DialogUtil;
import edu.uvm.ccts.common.util.FileUtil;
import edu.uvm.ccts.mlmeditor.mlm.MLMBuilder;
import edu.uvm.ccts.mlmeditor.mlm.model.Category;
import edu.uvm.ccts.mlmeditor.mlm.model.MLM;
import edu.uvm.ccts.mlmeditor.mlm.model.Slot;
import edu.uvm.ccts.mlmeditor.model.Config;
import edu.uvm.ccts.mlmeditor.model.SlotType;
import edu.uvm.ccts.mlmeditor.model.UICategory;
import edu.uvm.ccts.mlmeditor.model.UISlot;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * @author Matthew Storer
 */
public class GUI extends JFrame {
    private static final Log log = LogFactory.getLog(GUI.class);


//////////////////////////////////////////////////////////////////////////////////////////
// instance methods
//

    private File file = null;

    public GUI() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " attempting to set system look and feel - " + e.getMessage(), e);
        }

        Config.load("/config.xml");
        initComponents();
        tabbedPane.remove(testPane);        // todo : remove testPane entirely after testing
        loadPanels();
    }

    private void loadFile() {
        file = FileUtil.selectFileToOpen(this,
                new FileNameExtensionFilter("MLM Files", "mlm"),
                System.getProperty("user.dir"));

        try {
            MLM mlm = new MLMBuilder().build(file);

            for (UICategory cat : Config.getInstance().getCategories().values()) {
                Category mlmCat = mlm.getCategory(cat.getName());
                if (mlmCat != null) {
                    for (UISlot slot : cat.getSlots()) {
                        Slot mlmSlot = mlmCat.getSlot(slot.getName());
                        if (mlmSlot != null) {
                            slot.setInputComponentValue(mlmSlot.getData());
                            slot.validate();
                        }
                    }
                }
            }

            btnSave.setEnabled(true);

        } catch (Exception e) {
            log.error("loadFile : caught " + e.getClass().getName() + " loading file '" + file.getPath() + "' - " + e.getMessage(), e);
            DialogUtil.showErrorDialog(this, "failed to open '" + file.getName() + "' : " + e.getMessage());
        }
    }

    private void saveFile() {
        try {
            StringBuilder sb = new StringBuilder();
            for (UICategory cat : Config.getInstance().getCategories().values()) {
                sb.append(cat.getName()).append(":\n");

                for (UISlot slot : cat.getSlots()) {
                    sb.append("  ").append(slot.getName());

                    String value = slot.getInputComponentValue();
                    if (slot.getType() == SlotType.STRUCTURED) {
                        value = value.replaceAll("\n", "\n    ");
                        sb.append(":\n    ").append(value).append("\n  ;;\n");

                    } else {
                        sb.append(": ").append(value).append(";;\n");
                    }
                }
            }

            sb.append("end:\n");

            FileUtil.write(file.getCanonicalPath(), sb.toString(), false);

        } catch (Exception e) {
            log.error("saveFile : caught " + e.getClass().getName() + " saving file - " + e.getMessage(), e);
            DialogUtil.showErrorDialog(this, e.getMessage());
        }
    }

    private boolean mlmIsValid() {
        boolean validated = true;
        for (UICategory cat : Config.getInstance().getCategories().values()) {
            validated = cat.validate() && validated;
        }

        if ( ! validated ) {
            DialogUtil.showErrorDialog(this, "This MLM contains errors.  Please correct any errors and try again.");
        }

        return validated;
    }

    private void btnOpenFileActionPerformed() {
        loadFile();
    }

    private void btnSaveActionPerformed() {
        if (mlmIsValid()) {
            saveFile();
        }
    }

    private void btnSaveAsActionPerformed() {
        if (mlmIsValid()) {
            String filename = null;
            try {
                filename = file != null ?
                        file.getCanonicalPath() :
                        null;

            } catch (Exception e) {
                log.error("caught " + e.getClass().getName() + " getting canonical path for file {" + file + "}");
            }

            File f = FileUtil.selectFileToSave(this,
                    new FileNameExtensionFilter("MLM Files", "mlm"),
                    System.getProperty("user.dir"), filename);

            if (f != null) {
                file = f;
                saveFile();
            }
        }
    }

    private void loadPanels() {
        for (UICategory cat : Config.getInstance().getCategories().values()) {
            JPanel panel = new JPanel();

            GroupLayout layout = new GroupLayout(panel);
            panel.setLayout(layout);
            layout.setHorizontalGroup(buildHorizontalGroup(layout, cat));
            layout.setVerticalGroup(buildVerticalGroup(layout, cat));

            JScrollPane scrollPane = new JScrollPane();
            scrollPane.setViewportView(panel);
            tabbedPane.addTab(cat.getLabel(), scrollPane);
        }
    }

    private GroupLayout.Group buildHorizontalGroup(GroupLayout layout, UICategory cat) {
        GroupLayout.ParallelGroup labelGroup = layout.createParallelGroup();
        GroupLayout.ParallelGroup componentGroup = layout.createParallelGroup();

        for (UISlot slot : cat.getSlots()) {
            labelGroup.addComponent(slot.getLabelComponent());
            componentGroup.addComponent(slot.getInputComponent(), GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE);
            componentGroup.addComponent(slot.getMessageComponent(), GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE);
        }

        return layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(labelGroup)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(componentGroup)
                        .addGap(20, 20, 20));
    }

    private GroupLayout.Group buildVerticalGroup(GroupLayout layout, UICategory cat) {
        GroupLayout.SequentialGroup sg = layout.createSequentialGroup();

        sg.addContainerGap();

        for (UISlot slot : cat.getSlots()) {
            int size = slot.isMultiline() ? 120 : GroupLayout.DEFAULT_SIZE;

            sg.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(slot.getLabelComponent())
                    .addComponent(slot.getInputComponent(), GroupLayout.PREFERRED_SIZE, size, GroupLayout.PREFERRED_SIZE));
            sg.addComponent(slot.getMessageComponent());
            sg.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED);
        }

        sg.addContainerGap(10, Short.MAX_VALUE);

        return layout.createParallelGroup()
                .addGroup(sg);
    }

    private void btnQuitActionPerformed() {
        this.setVisible(false);
    }

    private void btnNewActionPerformed() {
        file = null;

        for (UICategory cat : Config.getInstance().getCategories().values()) {
            for (UISlot slot : cat.getSlots()) {
                slot.setInputComponentValue(slot.getDefaultValue());
            }
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license
        menuBar = new JMenuBar();
        fileMenu = new JMenu();
        btnNew = new JMenuItem();
        btnOpenFile = new JMenuItem();
        btnSave = new JMenuItem();
        btnSaveAs = new JMenuItem();
        btnQuit = new JMenuItem();
        tabbedPane = new JTabbedPane();
        testPane = new JScrollPane();
        panel = new JPanel();
        label1 = new JLabel();
        textField1 = new JTextField();
        label2 = new JLabel();
        textField2 = new JTextField();
        label3 = new JLabel();
        scrollPane1 = new JScrollPane();
        textArea1 = new JTextArea();
        message = new JLabel();
        message2 = new JLabel();

        //======== this ========
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("MLM Editor");
        Container contentPane = getContentPane();

        //======== menuBar ========
        {

            //======== fileMenu ========
            {
                fileMenu.setText("File");

                //---- btnNew ----
                btnNew.setText("New");
                btnNew.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnNewActionPerformed();
                    }
                });
                fileMenu.add(btnNew);

                //---- btnOpenFile ----
                btnOpenFile.setText("Open ...");
                btnOpenFile.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnOpenFileActionPerformed();
                    }
                });
                fileMenu.add(btnOpenFile);

                //---- btnSave ----
                btnSave.setText("Save");
                btnSave.setEnabled(false);
                btnSave.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnSaveActionPerformed();
                    }
                });
                fileMenu.add(btnSave);

                //---- btnSaveAs ----
                btnSaveAs.setText("Save As ...");
                btnSaveAs.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnSaveAsActionPerformed();
                    }
                });
                fileMenu.add(btnSaveAs);

                //---- btnQuit ----
                btnQuit.setText("Quit");
                btnQuit.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnQuitActionPerformed();
                    }
                });
                fileMenu.add(btnQuit);
            }
            menuBar.add(fileMenu);
        }
        setJMenuBar(menuBar);

        //======== tabbedPane ========
        {

            //======== testPane ========
            {

                //======== panel ========
                {
                    panel.setEnabled(false);

                    //---- label1 ----
                    label1.setText("label1:");

                    //---- textField1 ----
                    textField1.setBorder(LineBorder.createGrayLineBorder());

                    //---- label2 ----
                    label2.setText("this is a longer label:");

                    //---- label3 ----
                    label3.setText("text area:");

                    //======== scrollPane1 ========
                    {

                        //---- textArea1 ----
                        textArea1.setFont(new Font("Lucida Console", Font.PLAIN, 11));
                        scrollPane1.setViewportView(textArea1);
                    }

                    //---- message ----
                    message.setText("text");

                    //---- message2 ----
                    message2.setText("text");

                    GroupLayout panelLayout = new GroupLayout(panel);
                    panel.setLayout(panelLayout);
                    panelLayout.setHorizontalGroup(
                        panelLayout.createParallelGroup()
                            .addGroup(panelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(panelLayout.createParallelGroup()
                                    .addGroup(panelLayout.createParallelGroup()
                                        .addGroup(panelLayout.createSequentialGroup()
                                            .addComponent(label1)
                                            .addGap(100, 100, 100))
                                        .addGroup(GroupLayout.Alignment.TRAILING, panelLayout.createSequentialGroup()
                                            .addComponent(label2)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)))
                                    .addGroup(panelLayout.createSequentialGroup()
                                        .addComponent(label3)
                                        .addGap(83, 83, 83)))
                                .addGroup(panelLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                    .addComponent(message, GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE)
                                    .addComponent(scrollPane1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE)
                                    .addComponent(textField2, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE)
                                    .addComponent(message2, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE)
                                    .addComponent(textField1, GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE))
                                .addGap(208, 208, 208))
                    );
                    panelLayout.setVerticalGroup(
                        panelLayout.createParallelGroup()
                            .addGroup(panelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(panelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                    .addComponent(label1)
                                    .addComponent(textField1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(message)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                    .addComponent(label2)
                                    .addComponent(textField2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addGap(10, 10, 10)
                                .addComponent(message2)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panelLayout.createParallelGroup()
                                    .addComponent(label3)
                                    .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(201, Short.MAX_VALUE))
                    );
                }
                testPane.setViewportView(panel);
            }
            tabbedPane.addTab("text", testPane);
        }

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(tabbedPane, GroupLayout.DEFAULT_SIZE, 757, Short.MAX_VALUE)
                    .addContainerGap())
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(tabbedPane, GroupLayout.DEFAULT_SIZE, 399, Short.MAX_VALUE)
                    .addContainerGap())
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenuItem btnNew;
    private JMenuItem btnOpenFile;
    private JMenuItem btnSave;
    private JMenuItem btnSaveAs;
    private JMenuItem btnQuit;
    private JTabbedPane tabbedPane;
    private JScrollPane testPane;
    private JPanel panel;
    private JLabel label1;
    private JTextField textField1;
    private JLabel label2;
    private JTextField textField2;
    private JLabel label3;
    private JScrollPane scrollPane1;
    private JTextArea textArea1;
    private JLabel message;
    private JLabel message2;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
