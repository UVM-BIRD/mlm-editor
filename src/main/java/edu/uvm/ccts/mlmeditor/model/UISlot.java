/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MLM Editor.
 *
 * MLM Editor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MLM Editor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MLM Editor.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.mlmeditor.model;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by mstorer on 3/28/14.
 */
public class UISlot {
    private static final Log log = LogFactory.getLog(UISlot.class);

    private String name;
    private SlotType type;
    private boolean required;
    private String label = null;
    private String defaultValue = null;
    private boolean editable = true;
    private boolean multiline = false;
    private List<String> values = null;
    private SlotValidation slotValidation = null;

    private JLabel labelComponent = null;
    private Component inputComponent = null;
    private JLabel messageComponent = null;

    public UISlot(String name, SlotType type, boolean required) {
        this.name = name;
        this.type = type;
        this.required = required;
    }

    public JLabel getLabelComponent() {
        if (labelComponent == null) {
            labelComponent = new JLabel();

            String text = required ?
                    "* " + getLabel() + ":" :
                    getLabel() + ":";

            labelComponent.setText(text);
        }

        return labelComponent;
    }


    public JLabel getMessageComponent() {
        if (messageComponent == null) {
            messageComponent = new JLabel();
            messageComponent.setForeground(Color.RED);
            messageComponent.setVisible(false);
        }

        return messageComponent;
    }

    public Component getInputComponent() {
        if (inputComponent == null) {
            Component component = null;

            if ((type == SlotType.TEXT && multiline) || type == SlotType.STRUCTURED || type == SlotType.TEXT_LIST) {
                JScrollPane scrollPane = new JScrollPane();
                JTextArea textArea = new JTextArea();

                if (type == SlotType.STRUCTURED) {
                    textArea.setFont(new Font("Lucida Console", Font.PLAIN, 11));
                }

                component = textArea;                   // we don't want to set the validation listener on the scrollPane

                scrollPane.setViewportView(textArea);

                if (defaultValue != null) textArea.setText(defaultValue);

                inputComponent = scrollPane;

            } else if (type == SlotType.CODED_DATE) {
                Format format = new SimpleDateFormat("yyyy-MM-dd");
                JFormattedTextField textField = new JFormattedTextField(format);

                textField.setBorder(BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.GRAY),
                        BorderFactory.createEmptyBorder(2, 2, 2, 2)
                ));

                if (defaultValue != null) textField.setText(defaultValue);

                inputComponent = textField;

            } else if (type == SlotType.CODED && values != null) {
                JComboBox<String> comboBox = new JComboBox<String>();
                for (String value : values) {
                    comboBox.addItem(value);
                }

                if (defaultValue != null && values.contains(defaultValue)) {
                    comboBox.setSelectedItem(defaultValue);
                }

                inputComponent = comboBox;

            } else {
                JTextField textField = new JTextField();

                textField.setBorder(BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.GRAY),
                        BorderFactory.createEmptyBorder(2, 2, 2, 2)
                ));

                if (defaultValue != null) textField.setText(defaultValue);

                inputComponent = textField;
            }

            if (editable) {
                if (component != null) addValidationListener(component);
                else                   addValidationListener(inputComponent);

            } else {
                if (component != null) component.setEnabled(false);
                inputComponent.setEnabled(false);
            }
        }

        return inputComponent;
    }

    public boolean validate() {
        if (required || slotValidation != null) {
            String value = getInputComponentValue();

            try {
                if (required && value.trim().isEmpty()) {
                    throw new ValidationException(name, "is required");

                } else if (value.contains(";;")) {
                    throw new ValidationException(name, "cannot contain slot-terminator ';;'");

                } else if (slotValidation != null) {
                    slotValidation.validate(name, value);
                }

                showPassesValidation();

            } catch (ValidationException ve) {
                showFailsValidation(ve);
                return false;
            }
        }

        return true;
    }

    public void setInputComponentValue(String str) {
        if (inputComponent != null) {
            if (inputComponent instanceof JTextField) {
                ((JTextField) inputComponent).setText(str);

            } else if (inputComponent instanceof JScrollPane) {
                JScrollPane scrollPane = (JScrollPane) inputComponent;
                Component component = scrollPane.getViewport().getView();
                if (component instanceof JTextArea) {
                    JTextArea textArea = (JTextArea) component;
                    textArea.setText(str);
                    textArea.setCaretPosition(0);
                }

            } else if (inputComponent instanceof JComboBox) {
                JComboBox comboBox = (JComboBox) inputComponent;
                for (int i = 0; i < comboBox.getItemCount(); i ++) {
                    String value = String.valueOf(comboBox.getItemAt(i));
                    if (value.equalsIgnoreCase(str)) {
                        comboBox.setSelectedIndex(i);
                        break;
                    }
                }
            }
        }
    }

    public String getInputComponentValue() {
        if (inputComponent == null) {
            return "";

        } else if (inputComponent instanceof JTextField) {
            return ((JTextField) inputComponent).getText();

        } else if (inputComponent instanceof JScrollPane) {
            JScrollPane scrollPane = (JScrollPane) inputComponent;
            Component component = scrollPane.getViewport().getView();
            if (component instanceof JTextArea) {
                return ((JTextArea) component).getText();
            }

        } else if (inputComponent instanceof JComboBox) {
            return String.valueOf(((JComboBox) inputComponent).getSelectedItem());
        }

        log.error("getInputComponentValue : couldn't determine input component value for " +
                inputComponent.getClass().getName());

        return "";
    }


///////////////////////////////////////////////////////////////////////////////////////////////
// private methods
//

    private void addValidationListener(Component component) {
        component.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                validate();
            }
        });
    }

    private void showPassesValidation() {
        if (inputComponent instanceof JTextField) {
            JTextField textField = (JTextField) inputComponent;
            textField.setForeground(null);
            textField.setBorder(BorderFactory.createCompoundBorder(
                    BorderFactory.createLineBorder(Color.GRAY),
                    BorderFactory.createEmptyBorder(2, 2, 2, 2)
            ));

        } else if (inputComponent instanceof JScrollPane) {
            JScrollPane scrollPane = (JScrollPane) inputComponent;
            Component component = scrollPane.getViewport().getView();
            if (component instanceof JTextArea) {
                JTextArea textArea = (JTextArea) component;

                // todo : undo any formatting set in showFailsValidation() below
            }
        }

        messageComponent.setText(null);
        messageComponent.setVisible(false);
    }

    private void showFailsValidation(ValidationException e) {
        if (inputComponent instanceof JTextField) {
            JTextField textField = (JTextField) inputComponent;
            textField.setForeground(Color.RED);
            textField.setBorder(BorderFactory.createCompoundBorder(
                    BorderFactory.createLineBorder(Color.RED),
                    BorderFactory.createEmptyBorder(2, 2, 2, 2)
            ));

        } else if (inputComponent instanceof JScrollPane) {
            JScrollPane scrollPane = (JScrollPane) inputComponent;
            Component component = scrollPane.getViewport().getView();
            if (component instanceof JTextArea) {
                JTextArea textArea = (JTextArea) component;

                // todo : highlight row containing error for STRUCTURED slots; show plain error otherwise
            }
        }

        messageComponent.setText(e.getMessage());
        messageComponent.setVisible(true);
    }


/////////////////////////////////////////////////////////////////////////////////////////
// basic getters and setters
//

    public String getName() {
        return name;
    }

    public SlotType getType() {
        return type;
    }

    public boolean isRequired() {
        return required;
    }

    public String getLabel() {
        return label != null ?
                label :
                StringUtils.capitalize(name);
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public boolean isMultiline() {
        return multiline;
    }

    public void setMultiline(boolean multiline) {
        this.multiline = multiline;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public SlotValidation getSlotValidation() {
        return slotValidation;
    }

    public void setSlotValidation(SlotValidation slotValidation) {
        this.slotValidation = slotValidation;
    }
}
