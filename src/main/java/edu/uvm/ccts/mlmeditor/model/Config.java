/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MLM Editor.
 *
 * MLM Editor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MLM Editor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MLM Editor.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.mlmeditor.model;

import edu.uvm.ccts.common.util.ResourceUtil;
import edu.uvm.ccts.jmethodsig.JMethodSigBuilder;
import electric.xml.Document;
import electric.xml.Element;
import electric.xml.Elements;
import electric.xml.XPath;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mstorer on 3/28/14.
 */
public class Config {
    private static Config config = null;

    public static void load(String resource) {
        config = new Config(resource);
    }

    public static Config getInstance() {
        return config;
    }

    private Map<String, UICategory> categories = null;

    public Config(String resource) {
        try {
            Document doc = new Document(readResourceAsString(resource));
            populateCategories(doc);

        } catch (Exception e) {
            if (e instanceof RuntimeException) {
                throw (RuntimeException) e;

            } else {
                throw new RuntimeException(e);
            }
        }
    }

    public Map<String, UICategory> getCategories() {
        return categories;
    }

    private String readResourceAsString(String path) throws IOException {
        byte[] bytes = ResourceUtil.readResource(path);
        return bytes != null ?
                new String(bytes) :
                null;
    }

    private void populateCategories(Document doc) throws IOException {
        categories = new LinkedHashMap<String, UICategory>();

        Elements xmlCategories = doc.getElements(new XPath("mlmEditor/categories/category"));
        while (xmlCategories.hasMoreElements()) {
            Element xmlCategory = xmlCategories.next();
            String name = xmlCategory.getAttribute("name");
            List<UISlot> slots = buildSlots(xmlCategory);

            UICategory category = new UICategory(name, slots);

            categories.put(category.getName(), category);
        }
    }

    private List<UISlot> buildSlots(Element xmlCategory) throws IOException {
        List<UISlot> list = new ArrayList<UISlot>();

        Elements xmlSlots = xmlCategory.getElements(new XPath("slots/slot"));
        while (xmlSlots.hasMoreElements()) {
            Element xmlSlot = xmlSlots.next();

            UISlot slot = new UISlot(
                    xmlSlot.getAttribute("name"),
                    SlotType.valueOf(xmlSlot.getAttribute("type").toUpperCase()),
                    xmlSlot.getAttribute("required").equalsIgnoreCase("true"));

            if (xmlSlot.hasAttribute("label"))        slot.setLabel(xmlSlot.getAttribute("label"));
            if (xmlSlot.hasAttribute("defaultValue")) slot.setDefaultValue(xmlSlot.getAttribute("defaultValue"));
            if (xmlSlot.hasAttribute("editable"))     slot.setEditable(xmlSlot.getAttribute("editable").equalsIgnoreCase("true"));
            if (xmlSlot.hasAttribute("multiline"))    slot.setMultiline(xmlSlot.getAttribute("multiline").equalsIgnoreCase("true"));

            Elements xmlValues = xmlSlot.getElements(new XPath("values/value"));
            if (xmlValues.hasMoreElements()) {
                List<String> values = new ArrayList<String>();

                while (xmlValues.hasMoreElements()) {
                    Element xmlValue = xmlValues.next();
                    values.add(xmlValue.getTrimTextString());
                }

                slot.setValues(values);
            }

            Element xmlValidation = xmlSlot.getElement("validation");
            if (xmlValidation != null) {
                SlotValidation sv = new SlotValidation();

                if (xmlValidation.hasAttribute("minLength")) {
                    sv.setMinLength(Integer.parseInt(xmlValidation.getAttribute("minLength")));
                }

                if (xmlValidation.hasAttribute("maxLength")) {
                    sv.setMaxLength(Integer.parseInt(xmlValidation.getAttribute("maxLength")));
                }

                if (xmlValidation.hasAttribute("pattern")) {
                    sv.setPatternRegex(xmlValidation.getAttribute("pattern"));
                }

                if (xmlValidation.hasAttribute("validator")) {
                    sv.setValidator(JMethodSigBuilder.build(xmlValidation.getAttribute("validator")));
                }

                slot.setSlotValidation(sv);
            }

            list.add(slot);
        }

        return list;
    }
}
