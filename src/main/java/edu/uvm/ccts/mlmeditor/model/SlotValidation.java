/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MLM Editor.
 *
 * MLM Editor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MLM Editor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MLM Editor.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.mlmeditor.model;

import edu.uvm.ccts.common.exceptions.antlr.AntlrException;
import edu.uvm.ccts.jmethodsig.model.JMethodSig;

import java.lang.reflect.InvocationTargetException;
import java.util.regex.Pattern;

/**
 * Created by mstorer on 3/31/14.
 */
public class SlotValidation {
    private int minLength = 0;
    private int maxLength = Integer.MAX_VALUE;
    private String patternRegex = null;
    private Pattern pattern = null;
    private JMethodSig validator = null;


    public void validate(String name, String value) throws ValidationException {
        if (value == null) {
            throw new ValidationException(name, "cannot be empty");

        } else if (value.length() < minLength) {
            throw new ValidationException(name, "is too short (minimum length = " + minLength + " chars)");

        } else if (value.length() > maxLength) {
            throw new ValidationException(name, "is too long (maximum length = " + maxLength + " chars)");

        } else if (pattern != null) {
            if ( ! pattern.matcher(value).matches() ) {
                throw new ValidationException(name, "doesn't match pattern " + patternRegex);
            }

        } else if (validator != null) {
            try {
                Object[] args = new Object[]{value};
                validator.execute(args);

            } catch (InvocationTargetException e) {
                if (e.getTargetException() instanceof AntlrException) {
                    AntlrException ae = (AntlrException) e.getTargetException();
                    throw new ValidationException(name, ae.getMessage(), ae);

                } else {
                    throw new RuntimeException(e.getMessage(), e);
                }

            } catch (Exception e) {
                if (e instanceof RuntimeException) throw (RuntimeException) e;
                else                               throw new RuntimeException(e.getMessage(), e);
            }
        }
    }

    public void setMinLength(int minLength) {
        this.minLength = minLength;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

    public void setPatternRegex(String patternRegex) {
        this.patternRegex = patternRegex;
        pattern = Pattern.compile(patternRegex); //.replaceAll("\\\\", "\\\\\\\\"));
    }

    public void setValidator(JMethodSig validator) {
        this.validator = validator;
    }
}
