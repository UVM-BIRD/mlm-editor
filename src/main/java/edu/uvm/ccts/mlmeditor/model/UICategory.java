/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MLM Editor.
 *
 * MLM Editor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MLM Editor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MLM Editor.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.mlmeditor.model;

import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mstorer on 3/28/14.
 */
public class UICategory {
    private String name;
    private Map<String, UISlot> slots = new LinkedHashMap<String, UISlot>();

    public UICategory(String name, List<UISlot> slots) {
        this.name = name;
        for (UISlot slot : slots) {
            this.slots.put(slot.getName(), slot);
        }
    }

    public boolean validate() {
        boolean rval = true;
        for (UISlot slot : slots.values()) {
            rval = slot.validate() && rval;
        }
        return rval;
    }

    public String getName() {
        return name;
    }

    public String getLabel() {
        return StringUtils.capitalize(name);
    }

    public Collection<String> getSlotNames() {
        return slots.keySet();
    }

    public UISlot getSlot(String slotName) {
        return slots.get(slotName);
    }

    public Collection<UISlot> getSlots() { return slots.values(); }
}
