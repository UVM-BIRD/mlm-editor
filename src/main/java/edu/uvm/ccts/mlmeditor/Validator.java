/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MLM Editor.
 *
 * MLM Editor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MLM Editor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MLM Editor.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.mlmeditor;

import edu.uvm.ccts.arden.action.BailActionLexer;
import edu.uvm.ccts.arden.action.BailActionParser;
import edu.uvm.ccts.arden.action.antlr.ActionBaseVisitor;
import edu.uvm.ccts.arden.action.antlr.ActionLexer;
import edu.uvm.ccts.arden.action.antlr.ActionParser;
import edu.uvm.ccts.arden.data.BailDataLexer;
import edu.uvm.ccts.arden.data.BailDataParser;
import edu.uvm.ccts.arden.data.antlr.DataBaseVisitor;
import edu.uvm.ccts.arden.data.antlr.DataLexer;
import edu.uvm.ccts.arden.data.antlr.DataParser;
import edu.uvm.ccts.arden.evoke.BailEvokeLexer;
import edu.uvm.ccts.arden.evoke.BailEvokeParser;
import edu.uvm.ccts.arden.evoke.antlr.EvokeBaseVisitor;
import edu.uvm.ccts.arden.evoke.antlr.EvokeLexer;
import edu.uvm.ccts.arden.evoke.antlr.EvokeParser;
import edu.uvm.ccts.arden.logic.BailLogicLexer;
import edu.uvm.ccts.arden.logic.BailLogicParser;
import edu.uvm.ccts.arden.logic.antlr.LogicBaseVisitor;
import edu.uvm.ccts.arden.logic.antlr.LogicLexer;
import edu.uvm.ccts.arden.logic.antlr.LogicParser;
import edu.uvm.ccts.common.exceptions.antlr.AntlrException;
import edu.uvm.ccts.mlmeditor.model.ValidationException;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by mstorer on 3/28/14.
 */
public class Validator {
    private static final Log log = LogFactory.getLog(Validator.class);

    public static void validateData(String arden) throws AntlrException, IOException {
        if (arden != null && StringUtils.isNotEmpty(arden)) {
            InputStream inputStream = new ByteArrayInputStream(arden.getBytes());
            ANTLRInputStream input = new ANTLRInputStream(inputStream);
            DataLexer lexer = new BailDataLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            DataParser parser = new BailDataParser(tokens);
            ParseTree tree = parser.init();

            DataBaseVisitor visitor = new DataBaseVisitor();
            visitor.visit(tree);
        }
    }

    public static void validateLogic(String arden) throws ValidationException, IOException {
        if (arden != null && StringUtils.isNotEmpty(arden)) {
            InputStream inputStream = new ByteArrayInputStream(arden.getBytes());
            ANTLRInputStream input = new ANTLRInputStream(inputStream);
            LogicLexer lexer = new BailLogicLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            LogicParser parser = new BailLogicParser(tokens);
            ParseTree tree = parser.init();

            LogicBaseVisitor visitor = new LogicBaseVisitor();
            visitor.visit(tree);
        }
    }

    public static void validateEvoke(String arden) throws ValidationException, IOException  {
        if (arden != null && StringUtils.isNotEmpty(arden)) {
            InputStream inputStream = new ByteArrayInputStream(arden.getBytes());
            ANTLRInputStream input = new ANTLRInputStream(inputStream);
            EvokeLexer lexer = new BailEvokeLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            EvokeParser parser = new BailEvokeParser(tokens);
            ParseTree tree = parser.init();

            EvokeBaseVisitor visitor = new EvokeBaseVisitor();
            visitor.visit(tree);
        }
    }

    public static void validateAction(String arden) throws ValidationException, IOException  {
        if (arden != null && StringUtils.isNotEmpty(arden)) {
            InputStream inputStream = new ByteArrayInputStream(arden.getBytes());
            ANTLRInputStream input = new ANTLRInputStream(inputStream);
            ActionLexer lexer = new BailActionLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            ActionParser parser = new BailActionParser(tokens);
            ParseTree tree = parser.init();

            ActionBaseVisitor visitor = new ActionBaseVisitor();
            visitor.visit(tree);
        }
    }
}
