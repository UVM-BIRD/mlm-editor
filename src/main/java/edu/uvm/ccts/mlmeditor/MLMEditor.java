/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MLM Editor.
 *
 * MLM Editor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MLM Editor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MLM Editor.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.mlmeditor;

import edu.uvm.ccts.common.util.LicenseUtil;
import edu.uvm.ccts.mlmeditor.ui.GUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;

/**
 * Created by mstorer on 2/25/15.
 */
public class MLMEditor {
    private static final Log log = LogFactory.getLog(MLMEditor.class);

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " attempting to set system look and feel - " + e.getMessage(), e);
        }

        LicenseUtil.requireLicenseAcceptance(MLMEditor.class, "MLM Editor Licensing", "/gpl-3.0-standalone.html");

        new GUI().setVisible(true);
    }
}
