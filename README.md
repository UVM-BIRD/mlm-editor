# MLM Editor #

MLM Editor is a Java application that can be used to create, edit, and validate Arden-syntax Medical Logic Modules (MLMs).

### Building MLM Editor ###

Building the MLM Editor is accomplished by running the following:

    $ mvn package

After running this command, you will find the application JAR in the project's *target/* folder.

### Running the MLM Editor ###

Running the MLM Editor application can be accomplished by double-clicking on the JAR through a user interface, or by executing the following from the command line:

    $ java -jar target/mlm-editor-1.0.jar

### License and Copyright ###

The MLM Editor application is Copyright 2015 [The University of Vermont and State Agricultural College](https://www.uvm.edu/), [Vermont Oxford Network](https://public.vtoxford.org/), and [The University of Vermont Medical Center](https://www.uvmhealth.org/medcenter/Pages/default.aspx).  All rights reserved.

The MLM Editor application is licensed under the terms of the [GNU General Public License (GPL) version 3](https://www.gnu.org/licenses/gpl-3.0-standalone.html).